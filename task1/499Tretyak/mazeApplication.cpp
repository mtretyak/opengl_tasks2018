#include "mazeApplication.h"

#include <fstream>

MazeApplication::MazeApplication()
{
    std::fstream stream("./499TretyakData/Maze.txt", std::ios::in);
    _maze = std::make_shared<Maze>(stream);
    stream.close();

    _usualCam    = std::make_shared<FreeCameraMover>();
    _mazeCam     = std::make_shared<MazeCamera>(_maze);
    _cameraMover = _usualCam;
}

void MazeApplication::makeScene()
{
    Application::makeScene();

    _shader = std::make_shared<ShaderProgram>("499TretyakData/shaderNormal.vert", "499TretyakData/shader.frag");

    //_floor = makeFloor();//makeCube(0.5);
    //_floor->setModelMatrix(glm::translate(glm::mat4(1.0f), glm::vec3(0., 0., 0.)));

    _mazeMesh = _maze->makeMazeMech();
    _mazeMesh->setModelMatrix(glm::translate(glm::mat4(1.0f), glm::vec3(0., 0., 0.)));
}

void MazeApplication::draw()
{
    Application::draw();

    int width, height;
    glfwGetFramebufferSize(_window, &width, &height);

    glViewport(0, 0, width, height);

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    //Устанавливаем шейдер.
    _shader->use();

    //Устанавливаем общие юниформ-переменные
    _shader->setMat4Uniform("viewMatrix", _camera.viewMatrix);
    _shader->setMat4Uniform("projectionMatrix", _camera.projMatrix);

    _shader->setMat4Uniform("modelMatrix", _mazeMesh->modelMatrix());

    _mazeMesh->draw();
}

void MazeApplication::handleKey(int key, int scancode, int action, int mods)
{
    Application::handleKey(key, scancode, action, mods);

    if (action == GLFW_PRESS)
    {
        if (key == GLFW_KEY_1)
        {
            _cameraMover = _usualCam;
        }
        else if (key == GLFW_KEY_2)
        {
            _cameraMover = _mazeCam;
        }
    }
}