#pragma once

#include <fstream>
#include <vector>
#include <glm/glm.hpp>
#include "common/Mesh.hpp"

class Maze {
public:
    Maze(std::fstream& stream);
    MeshPtr makeMazeMech();
    void makeRectangle(std::vector<glm::vec3>& vertices, std::vector<glm::vec3>& normals,
                             glm::vec3 p1, glm::vec3 p2, glm::vec3 p3, glm::vec3 p4, glm::vec3 n);

    std::vector<std::vector<bool> > getWalls() const;

private:
    int _y;
    int _x;

    std::vector<std::vector<bool> > _ceiling;
    std::vector<std::vector<bool> > _walls;
    std::vector<std::vector<bool> > _floor;
};
