#include <cstdio>
#include <cstdlib>

#include "maze.h"

#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include <GL/glew.h>

#include <map>
#include <memory>


Maze::Maze(std::fstream& stream)
{
    stream >> _x >> _y;
    _floor.resize(_x);
    _walls.resize(_x);
    _ceiling.resize(_x);

    for (auto x = 0; x < _x; ++x)
    {
        _floor[x].resize(_y, false);
        _walls[x].resize(_y, false);
        _ceiling[x].resize(_y, false);
    }

    for (auto y = 0; y < _y; ++y)
    {
        for (auto x = 0; x < _x; ++x)
        {
            char curChar;
            stream >> curChar;
            switch (curChar)
            {
                case 'X' :
                    _walls[x][y] = true;
                    break;
                case '0' :
                    _floor[x][y] = true;

            }
            _ceiling[x][y] = true;
        }
    }
}

MeshPtr Maze::makeMazeMech()
{
    std::vector<glm::vec3> vertices;
    std::vector<glm::vec3> normals;

    for (auto x = 0; x < _x; ++x)
    {
        for (auto y = 0; y < _y; ++y)
        {
            if (x == 0 && _walls[x][y])
            {
                    makeRectangle(vertices, normals,
                                  glm::vec3(x, y + 1, 0), glm::vec3(x, y + 1, 1),
                                  glm::vec3(x, y, 1), glm::vec3(x, y, 0), glm::vec3(-1, 0, 0));
            }

            if (x == _x - 1 && _walls[x][y])
            {
                makeRectangle(vertices, normals,
                              glm::vec3(x + 1, y, 0), glm::vec3(x + 1, y, 1),
                              glm::vec3(x + 1, y + 1, 1), glm::vec3(x + 1, y + 1, 0), glm::vec3(1, 0, 0));
            }

            if (y == 0 && _walls[x][y])
            {
                makeRectangle(vertices, normals,
                              glm::vec3(x, y, 0), glm::vec3(x, y, 1),
                              glm::vec3(x + 1, y, 1), glm::vec3(x + 1, y, 0), glm::vec3(0, -1, 0));
            }

            if (y == _y - 1 && _walls[x][y])
            {
                    makeRectangle(vertices, normals,
                                  glm::vec3(x + 1, y + 1, 0), glm::vec3(x + 1, y + 1, 1),
                                  glm::vec3(x, y + 1, 1), glm::vec3(x, y + 1, 0), glm::vec3(0, 1, 0));
            }

            if (_floor[x][y])
            {
                if (y > 0)
                    if (_walls[x][y - 1])
                        makeRectangle(vertices, normals,
                                      glm::vec3(x, y, 0), glm::vec3(x, y, 1),
                                      glm::vec3(x + 1, y, 1), glm::vec3(x + 1, y, 0), glm::vec3(0, -1, 0));
                if (y < _y - 1)
                    if (_walls[x][y + 1])
                        makeRectangle(vertices, normals,
                                      glm::vec3(x + 1, y + 1, 0), glm::vec3(x + 1, y + 1, 1),
                                      glm::vec3(x, y + 1, 1), glm::vec3(x, y + 1, 0), glm::vec3(0, 1, 0));
                if (x > 0)
                    if (_walls[x - 1][y])
                        makeRectangle(vertices, normals,
                                      glm::vec3(x, y + 1, 0), glm::vec3(x, y + 1, 1),
                                      glm::vec3(x, y, 1), glm::vec3(x, y, 0), glm::vec3(-1, 0, 0));
                if (x < _x - 1)
                    if (_walls[x + 1][y])
                        makeRectangle(vertices, normals,
                                      glm::vec3(x + 1, y, 0), glm::vec3(x + 1, y, 1),
                                      glm::vec3(x + 1, y + 1, 1), glm::vec3(x + 1, y + 1, 0), glm::vec3(1, 0, 0));
            }

            makeRectangle(vertices, normals,
                          glm::vec3(x + 1, y, 0), glm::vec3(x, y, 0),
                          glm::vec3(x, y + 1, 0), glm::vec3(x + 1, y + 1, 0), glm::vec3(0, 0, 1));

            makeRectangle(vertices, normals,
                          glm::vec3(x + 1, y, 1), glm::vec3(x, y, 1),
                          glm::vec3(x, y + 1, 1), glm::vec3(x + 1, y + 1, 1), glm::vec3(0, 0, -1));
        }
    }

    DataBufferPtr buf0 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    buf0->setData(vertices.size() * sizeof(float) * 3, vertices.data());

    DataBufferPtr buf1 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    buf1->setData(normals.size() * sizeof(float) * 3, normals.data());

    MeshPtr mesh = std::make_shared<Mesh>();
    mesh->setAttribute(0, 3, GL_FLOAT, GL_FALSE, 0, 0, buf0);
    mesh->setAttribute(1, 3, GL_FLOAT, GL_FALSE, 0, 0, buf1);
    mesh->setPrimitiveType(GL_TRIANGLES);
    mesh->setVertexCount(vertices.size());

    return mesh;
}

std::vector<std::vector<bool> > Maze::getWalls() const
{
    return _walls;
}

void Maze::makeRectangle(std::vector<glm::vec3>& vertices, std::vector<glm::vec3>& normals,
                         glm::vec3 p1, glm::vec3 p2, glm::vec3 p3, glm::vec3 p4, glm::vec3 n)
{
    vertices.push_back(p1);
    vertices.push_back(p2);
    vertices.push_back(p3);

    vertices.push_back(p1);
    vertices.push_back(p3);
    vertices.push_back(p4);

    for(auto i = 0; i < 6; ++i)
        normals.push_back(n);
}