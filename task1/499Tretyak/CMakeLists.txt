include_directories(common)

set(SRC_FILES
    Main.cpp
    mazeApplication.cpp
    maze.cpp
    mazeCamera.cpp
    common/Application.cpp
    common/Camera.cpp
    common/Mesh.cpp
    common/ShaderProgram.cpp
)


set(HEADER_FILES
    Main.h
    mazeApplication.h
    maze.h
    mazeCamera.h
    common/Application.hpp
    common/Camera.hpp
    common/Mesh.hpp
    common/ShaderProgram.hpp
)

MAKE_TASK(499Tretyak 1 "${SRC_FILES}")
