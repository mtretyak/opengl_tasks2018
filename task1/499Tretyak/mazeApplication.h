#ifndef STUDENTTASKS2017_MAZEAPPLICATION_H
#define STUDENTTASKS2017_MAZEAPPLICATION_H

#include "common/Application.hpp"
#include "common/Mesh.hpp"
#include "common/ShaderProgram.hpp"

#include "mazeCamera.h"
#include "maze.h"
#include <vector>

class MazeApplication : public Application
{
public:
    MazeApplication();
    void makeScene() override;
    void draw() override;
    void handleKey(int key, int scancode, int action, int mods) override;

private:

private:
    MeshPtr                     _mazeMesh;
    std::shared_ptr<Maze>       _maze;
    ShaderProgramPtr            _shader;

    std::shared_ptr<FreeCameraMover> _usualCam;
    std::shared_ptr<MazeCamera>      _mazeCam;

    std::vector<std::vector<bool> > floorCoord;
    std::vector<std::vector<bool> > wallCoord;
};


#endif //STUDENTTASKS2017_MAZEAPPLICATION_H