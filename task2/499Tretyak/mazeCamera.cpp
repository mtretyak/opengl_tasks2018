//
// Created by misha on 15.03.18.
//



#include <iostream>
#include "mazeCamera.h"

MazeCamera::MazeCamera(std::shared_ptr<Maze> maze) :
        CameraMover(),
        _pos(1.5f, 0.25f, 0.1f)
{
    _maze = maze;
    //Нам нужно как-нибудь посчитать начальную ориентацию камеры
    _rot = glm::toQuat(glm::lookAt(_pos, glm::vec3(1.0f, 0.0f, 0.1f), glm::vec3(0.0f, 0.0f, 1.0f)));
}

void MazeCamera::handleKey(GLFWwindow* window, int key, int scancode, int action, int mods)
{

}

void MazeCamera::handleMouseMove(GLFWwindow* window, double xpos, double ypos)
{
    int state = glfwGetMouseButton(window, GLFW_MOUSE_BUTTON_LEFT);
    if (state == GLFW_PRESS)
    {
        double dx = xpos - _oldXPos;
        double dy = ypos - _oldYPos;

        //Добавляем небольшой поворов вокруг вертикальной оси
        glm::vec3 upDir(0.0f, 0.0f, 1.0f);
        _rot *= glm::angleAxis(static_cast<float>(dx * 0.005), upDir);
    }

    _oldXPos = xpos;
    _oldYPos = ypos;
}

void MazeCamera::handleScroll(GLFWwindow* window, double xoffset, double yoffset)
{
}

void MazeCamera::update(GLFWwindow* window, double dt)
{
    float speed = 2.0f;
    float oldX = _pos.x;
    float oldY = _pos.y;

    //Получаем текущее направление "вперед" в мировой системе координат
    glm::vec3 forwDir = glm::vec3(0.0f, 0.0f, -1.0f) * _rot;

    //Получаем текущее направление "вправо" в мировой системе координат
    glm::vec3 rightDir = glm::vec3(1.0f, 0.0f, 0.0f) * _rot;

    //Двигаем камеру вперед/назад
    if (glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS)
    {
        _pos += forwDir * speed * static_cast<float>(dt);
    }
    if (glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS)
    {
        _pos -= forwDir * speed * static_cast<float>(dt);
    }
    if (glfwGetKey(window, GLFW_KEY_A) == GLFW_PRESS)
    {
        _pos -= rightDir * speed * static_cast<float>(dt);
    }
    if (glfwGetKey(window, GLFW_KEY_D) == GLFW_PRESS)
    {
        _pos += rightDir * speed * static_cast<float>(dt);
    }

    const double MARGIN = 0.15;
    if (isOutOfBorders(_pos.x - MARGIN, _pos.y + MARGIN) || isOutOfBorders(_pos.x + MARGIN, _pos.y + MARGIN) ||
            isOutOfBorders(_pos.x + MARGIN, _pos.y - MARGIN) || isOutOfBorders(_pos.x - MARGIN, _pos.y - MARGIN))
    {
        _pos.x = oldX;
        _pos.y = oldY;
    }

    //-----------------------------------------

    //Соединяем перемещение и поворот вместе
    _camera.viewMatrix = glm::toMat4(-_rot) * glm::translate(-_pos);

    //-----------------------------------------

    int width, height;
    glfwGetFramebufferSize(window, &width, &height);

    //Обновляем матрицу проекции на случай, если размеры окна изменились
    _camera.projMatrix = glm::perspective(glm::radians(45.0f), (float)width / height, 0.1f, 100.f);
}

bool MazeCamera::isOutOfBorders(float x, float y)
{
    auto walls = _maze->getWalls();
    if (walls.empty())
        return true;

    if (x >= walls.size() || x < 0 || y >= walls[0].size() || y < 0)
        return true;

    return _maze->getWalls()[int(x)][int(y)];
}

glm::vec3 MazeCamera::getPosition()
{
    return _pos;
}