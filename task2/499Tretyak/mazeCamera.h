#ifndef STUDENTTASKS2017_MAZECAMERA_H
#define STUDENTTASKS2017_MAZECAMERA_H

#define GLM_FORCE_RADIANS
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include <memory>

#include <Camera.hpp>

#include <glm/gtx/transform.hpp>
#include <glm/gtx/quaternion.hpp>

#include "maze.h"

class MazeCamera : public CameraMover
{
public:
    MazeCamera(std::shared_ptr<Maze> maze);

    void handleKey(GLFWwindow* window, int key, int scancode, int action, int mods) override;
    void handleMouseMove(GLFWwindow* window, double xpos, double ypos) override;
    void handleScroll(GLFWwindow* window, double xoffset, double yoffset) override;
    void update(GLFWwindow* window, double dt) override;

    glm::vec3 getPosition();

protected:
    glm::vec3 _pos;
    glm::quat _rot;

    //Положение курсора мыши на предыдущем кадре
    double _oldXPos = 0.0;
    double _oldYPos = 0.0;

private:
    bool isOutOfBorders(float x, float y);

private:
    std::shared_ptr<Maze> _maze;
};


#endif //STUDENTTASKS2017_MAZECAMERA_H
