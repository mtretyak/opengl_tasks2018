#ifndef STUDENTTASKS2017_MAZEAPPLICATION_H
#define STUDENTTASKS2017_MAZEAPPLICATION_H

#include "common/Application.hpp"
#include "common/Mesh.hpp"
#include "common/ShaderProgram.hpp"

#include "mazeCamera.h"
#include "maze.h"
#include <vector>
#include <Texture.hpp>

class MazeApplication : public Application
{
public:
    MazeApplication();
    void makeScene() override;
    void updateGUI() override;
    void draw() override;
    void handleKey(int key, int scancode, int action, int mods) override;

private:
    void createFloor();
    void createCeil();
    void createWalls();
    void makeRectangle(std::vector<glm::vec3>& vertices, std::vector<glm::vec3>& normals,
                       std::vector<glm::vec2>& texcoords,  std::vector<glm::vec3>& tangents, glm::vec3 p1,
                       glm::vec3 p2, glm::vec3 p3, glm::vec3 p4, glm::vec3 n, glm::vec3 t);
    MeshPtr makeMeshRectangle(glm::vec3 p1, glm::vec3 p2, glm::vec3 p3, glm::vec3 p4, glm::vec3 n, glm::vec3 t);

private:
    ShaderProgramPtr _markerShader;
    ShaderProgramPtr _shader;

    std::vector<std::pair<MeshPtr, int> >  _wallMeshes;
    MeshPtr               _floorMesh;
    MeshPtr               _ceilMesh;
    MeshPtr               _marker;

    std::shared_ptr<Maze> _maze;

    std::shared_ptr<FreeCameraMover> _usualCam;
    std::shared_ptr<MazeCamera>      _mazeCam;

    glm::vec3 _lightAmbientColor;
    glm::vec3 _lightDiffuseColor;
    glm::vec3 _lightSpecularColor;
    float     _attenuation = 1.0f;

    int _lightType;

    TexturePtr _brickTexture;
    TexturePtr _brickNormalTexture;
    TexturePtr _floorTexture;
    TexturePtr _floorNormalTexture;
    TexturePtr _poster1Texture;
    TexturePtr _poster2Texture;
    GLuint _sampler;
};


#endif //STUDENTTASKS2017_MAZEAPPLICATION_H