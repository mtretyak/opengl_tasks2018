#include <cstdio>
#include <cstdlib>

#include "maze.h"

#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include <GL/glew.h>

#include <map>
#include <memory>


Maze::Maze(std::fstream& stream)
{
    stream >> _x >> _y;
    _floor.resize(_x);
    _walls.resize(_x);
    _ceiling.resize(_x);

    for (auto x = 0; x < _x; ++x)
    {
        _floor[x].resize(_y, false);
        _walls[x].resize(_y, false);
        _ceiling[x].resize(_y, false);
    }

    for (auto y = 0; y < _y; ++y)
    {
        for (auto x = 0; x < _x; ++x)
        {
            char curChar;
            stream >> curChar;
            switch (curChar)
            {
                case 'X':
                    _walls[x][y] = -1;
                    break;
                case '1':
                    _walls[x][y] = 1;
                    break;
                case '2':
                    _walls[x][y] = 2;
                    break;
                case '0' :
                    _floor[x][y] = true;

            }
            _ceiling[x][y] = true;
        }
    }
}

std::vector<std::vector<int> > Maze::getWalls() const
{
    return _walls;
}

std::vector<std::vector<bool> > Maze::getFloor() const
{
    return _floor;
}


int Maze::x() const
{
    return _x;
}

int Maze::y() const
{
    return _y;
}