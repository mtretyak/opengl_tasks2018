#pragma once

#include <fstream>
#include <vector>
#include <glm/glm.hpp>
#include "common/Mesh.hpp"

class Maze {
public:
    Maze(std::fstream& stream);

    std::vector<std::vector<int> > getWalls() const;
    std::vector<std::vector<bool> > getFloor() const;
    int x() const;
    int y() const;

private:
    int _y;
    int _x;

    std::vector<std::vector<bool> > _ceiling;
    std::vector<std::vector<int> > _walls;// -1 - no poster, 1,2 - posters
    std::vector<std::vector<bool> > _floor;
};
