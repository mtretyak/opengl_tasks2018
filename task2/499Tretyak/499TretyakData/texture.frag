/**
Использование текстуры в качестве коэффициента отражения света
*/

#version 330

uniform sampler2D diffuseTex;
uniform sampler2D normalTex;

struct LightInfo
{
	vec3 pos; //положение источника света в системе координат ВИРТУАЛЬНОЙ КАМЕРЫ!
	vec3 La; //цвет и интенсивность окружающего света
	vec3 Ld; //цвет и интенсивность диффузного света
	vec3 Ls; //цвет и интенсивность бликового света
	float attenuation;
};
uniform LightInfo light;
uniform int isPoster = 0;

in vec3 normalCamSpace; //нормаль в системе координат камеры (интерполирована между вершинами треугольника)
in vec3 tangentCamSpace; //нормаль в системе координат камеры (интерполирована между вершинами треугольника)
in vec4 posCamSpace; //координаты вершины в системе координат камеры (интерполированы между вершинами треугольника)
in vec2 texCoord; //текстурные координаты (интерполирована между вершинами треугольника)

out vec4 fragColor; //выходной цвет фрагмента

const vec3 Ks = vec3(1.0, 1.0, 1.0); //Коэффициент бликового отражения
const float shininess = 128.0;

void main()
{
	vec3 diffuseColor = texture(diffuseTex, texCoord).rgb;

	vec3 normal1;
	if (isPoster > 0.1)
	{
		normal1 = normalize(normalCamSpace);
	}
	else
	{
		normal1 = texture(normalTex, texCoord).rgb * 2.0 - 1.0;

		vec3 normal = normalize(normalCamSpace);
		vec3 tangent = normalize(tangentCamSpace);
		vec3 bitangent = normalize(cross(normal, tangent));
	
		normal1 = normalize(normal1.x * tangent + normal1.y * bitangent + normal1.z * normal);
	}

	vec3 viewDirection = normalize(-posCamSpace.xyz); //направление на виртуальную камеру (она находится в точке (0.0, 0.0, 0.0))
	
	float distance = length(light.pos - posCamSpace.xyz);

    	float attenuationCoef = 1.0 / (1. + light.attenuation * distance);

	vec3 lightDirCamSpace = normalize(light.pos - posCamSpace.xyz); //направление на источник света	

	float NdotL = max(dot(normal1, lightDirCamSpace.xyz), 0.0); //скалярное произведение (косинус)

	vec3 color = diffuseColor * (light.La + light.Ld * NdotL * attenuationCoef);

	if (NdotL > 0.0)
	{			
		vec3 halfVector = normalize(lightDirCamSpace.xyz + viewDirection); //биссектриса между направлениями на камеру и на источник света

		float blinnTerm = max(dot(normal1, halfVector), 0.0); //интенсивность бликового освещения по Блинну				
		blinnTerm = pow(blinnTerm, shininess); //регулируем размер блика
		color += light.Ls * Ks * blinnTerm;
	}

	fragColor = vec4(color, 1.0);
}
