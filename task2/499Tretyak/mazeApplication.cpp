#include "mazeApplication.h"

#include <iostream>

MazeApplication::MazeApplication()
{
    std::fstream stream("./499TretyakData/Maze.txt", std::ios::in);
    _maze = std::make_shared<Maze>(stream);
    stream.close();

    _usualCam    = std::make_shared<FreeCameraMover>();
    _mazeCam     = std::make_shared<MazeCamera>(_maze);
    _cameraMover = _usualCam;
}

void MazeApplication::makeScene()
{
    Application::makeScene();

    createFloor();
    createCeil();
    createWalls();
    _lightType = 0;
    _marker   = makeSphere(0.1f);

    _shader = std::make_shared<ShaderProgram>("499TretyakData/texture.vert",
                                              "499TretyakData/texture.frag");
    _markerShader = std::make_shared<ShaderProgram>("499TretyakData/marker.vert", "499TretyakData/marker.frag");


    _lightAmbientColor = glm::vec3(0.2, 0.2, 0.2);
    _lightDiffuseColor = glm::vec3(0.8, 0.8, 0.8);
    _lightSpecularColor = glm::vec3(1.0, 1.0, 1.0);
    _attenuation = .1f;

    _brickTexture = loadTexture("499TretyakData/Brick.jpg", SRGB::NO);
    _brickNormalTexture = loadTexture("499TretyakData/BrickNormal.jpg");
    _floorTexture = loadTexture("499TretyakData/Floor.jpg", SRGB::NO);
    _floorNormalTexture = loadTexture("499TretyakData/FloorNormal.jpg");
    _poster1Texture = loadTexture("499TretyakData/Dota1.jpg");
    _poster2Texture = loadTexture("499TretyakData/poster2.jpg");

    //=========================================================
    //Инициализация сэмплера, объекта, который хранит параметры чтения из текстуры
    glGenSamplers(1, &_sampler);
    glSamplerParameteri(_sampler, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glSamplerParameteri(_sampler, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glSamplerParameteri(_sampler, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glSamplerParameteri(_sampler, GL_TEXTURE_WRAP_T, GL_REPEAT);
}

void MazeApplication::updateGUI()
{
    Application::updateGUI();
    ImGui::SetNextWindowPos(ImVec2(0, 0), ImGuiSetCond_FirstUseEver);
    if (ImGui::Begin("Maze, Tretyak", NULL, ImGuiWindowFlags_AlwaysAutoResize))
    {
        ImGui::Text("FPS %.1f", ImGui::GetIO().Framerate);

        if (ImGui::CollapsingHeader("Light"))
        {
            ImGui::ColorEdit3("ambient", glm::value_ptr(_lightAmbientColor));
            ImGui::ColorEdit3("diffuse", glm::value_ptr(_lightDiffuseColor));
            ImGui::ColorEdit3("specular", glm::value_ptr(_lightSpecularColor));

            ImGui::SliderFloat("attenuation", &_attenuation, 0.01f, 10.0f);

            ImGui::Text("FPS %.1f", ImGui::GetIO().Framerate);
        }
    }
    ImGui::End();
}

void MazeApplication::draw()
{
    Application::draw();

    int width, height;
    glfwGetFramebufferSize(_window, &width, &height);

    glViewport(0, 0, width, height);

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    //Устанавливаем шейдер.
    _shader->use();

    //Устанавливаем общие юниформ-переменные
    _shader->setMat4Uniform("viewMatrix", _camera.viewMatrix);
    _shader->setMat4Uniform("projectionMatrix", _camera.projMatrix);
    auto lightPos = _lightType == 0 ? glm::vec3(_maze->x() / 2.0, _maze->y() / 2., 2.) :
                    _mazeCam->getPosition() + glm::vec3(0., 0., 0.5);

    _shader->setVec3Uniform("light.pos", _camera.viewMatrix * glm::vec4(lightPos, 1.0));
    _shader->setVec3Uniform("light.La", _lightAmbientColor);
    _shader->setVec3Uniform("light.Ld", _lightDiffuseColor);
    _shader->setVec3Uniform("light.Ls", _lightSpecularColor);
    _shader->setFloatUniform("light.attenuation", _attenuation);

    glActiveTexture(GL_TEXTURE0);  //текстурный юнит 0
    glBindSampler(0, _sampler);
    _floorTexture->bind();

    glActiveTexture(GL_TEXTURE1);
    glBindSampler(1, _sampler);
    _floorNormalTexture->bind();

    glActiveTexture(GL_TEXTURE2);
    glBindSampler(2, _sampler);
    _brickTexture->bind();

    glActiveTexture(GL_TEXTURE3);
    glBindSampler(3, _sampler);
    _brickNormalTexture->bind();

    glActiveTexture(GL_TEXTURE4);
    glBindSampler(4, _sampler);
    _poster1Texture->bind();

    glActiveTexture(GL_TEXTURE5);
    glBindSampler(5, _sampler);
    _poster2Texture->bind();

    _shader->setIntUniform("diffuseTex", 0);
    _shader->setIntUniform("normalTex", 1);
    {
        _shader->setMat4Uniform("modelMatrix", _floorMesh->modelMatrix());
        _shader->setMat3Uniform("normalToCameraMatrix",
                                                glm::transpose(glm::inverse(glm::mat3(_camera.viewMatrix * _floorMesh->modelMatrix()))));

        _floorMesh->draw();
    }

    _shader->setIntUniform("diffuseTex", 0);
    _shader->setIntUniform("normalTex", 1);
    {
        _shader->setMat4Uniform("modelMatrix", _ceilMesh->modelMatrix());
        _shader->setMat3Uniform("normalToCameraMatrix",
                                glm::transpose(glm::inverse(glm::mat3(_camera.viewMatrix * _ceilMesh->modelMatrix()))));

        _ceilMesh->draw();
    }

    _shader->setIntUniform("normalTex", 3);
    for (auto meshPtr: _wallMeshes)
    {
        if (meshPtr.second == -1)
        {
            _shader->setIntUniform("isPoster", 0);
            _shader->setIntUniform("diffuseTex", 2);
        }
        else if (meshPtr.second == 1)
        {
            _shader->setIntUniform("isPoster", 1);
            _shader->setIntUniform("diffuseTex", 4);
        }
        else
        {
            _shader->setIntUniform("isPoster", 1);
            _shader->setIntUniform("diffuseTex", 5);
        }
        _shader->setMat4Uniform("modelMatrix", meshPtr.first->modelMatrix());
        _shader->setMat3Uniform("normalToCameraMatrix",
                                glm::transpose(glm::inverse(glm::mat3(_camera.viewMatrix * meshPtr.first->modelMatrix()))));

        meshPtr.first->draw();
    }

    _shader->setIntUniform("isPoster", 0);

    {
        _markerShader->use();
        _markerShader->setMat4Uniform("mvpMatrix", _camera.projMatrix * _camera.viewMatrix *
                                                   glm::translate(glm::mat4(1.0f), lightPos));
        _markerShader->setVec4Uniform("color", glm::vec4(_lightDiffuseColor, 1.0f));
        _marker->draw();
    }

    //Отсоединяем сэмплер и шейдерную программу
    glBindSampler(0, 0);
    glUseProgram(0);
}

void MazeApplication::handleKey(int key, int scancode, int action, int mods)
{
    Application::handleKey(key, scancode, action, mods);

    if (action == GLFW_PRESS)
    {
        if (key == GLFW_KEY_1)
        {
            _cameraMover = _usualCam;
        }
        else if (key == GLFW_KEY_2)
        {
            _cameraMover = _mazeCam;
        }
        else if (key == GLFW_KEY_3)
        {
            _lightType = 0;
        }
        else if (key == GLFW_KEY_4)
        {
            _lightType = 1;
        }
    }
}

void MazeApplication::createFloor()
{
    std::vector<glm::vec3> vertices;
    std::vector<glm::vec3> normals;
    std::vector<glm::vec2> texcoords;
    std::vector<glm::vec3> tangents;
    for (auto x = 0; x < _maze->x(); ++x)
    {
        for (auto y = 0; y < _maze->y(); ++y)
        {

            makeRectangle(vertices, normals, texcoords, tangents,
                          glm::vec3(x + 1, y, 0), glm::vec3(x, y, 0),
                          glm::vec3(x, y + 1, 0), glm::vec3(x + 1, y + 1, 0), glm::vec3(0, 0, 1), glm::vec3(1, 0, 0));
        }
    }

    DataBufferPtr buf0 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    buf0->setData(vertices.size() * sizeof(float) * 3, vertices.data());

    DataBufferPtr buf1 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    buf1->setData(normals.size() * sizeof(float) * 3, normals.data());

    DataBufferPtr buf2 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    buf2->setData(texcoords.size() * sizeof(float) * 2, texcoords.data());

    DataBufferPtr buf3 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    buf3->setData(tangents.size() * sizeof(float) * 3, tangents.data());

    _floorMesh = std::make_shared<Mesh>();
    _floorMesh->setAttribute(0, 3, GL_FLOAT, GL_FALSE, 0, 0, buf0);
    _floorMesh->setAttribute(1, 3, GL_FLOAT, GL_FALSE, 0, 0, buf1);
    _floorMesh->setAttribute(2, 2, GL_FLOAT, GL_FALSE, 0, 0, buf2);
    _floorMesh->setAttribute(3, 3, GL_FLOAT, GL_FALSE, 0, 0, buf3);
    _floorMesh->setPrimitiveType(GL_TRIANGLES);
    _floorMesh->setVertexCount(vertices.size());

    _floorMesh->setModelMatrix(glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, 0.0f, 0.0f)));
}

void MazeApplication::createCeil()
{
    std::vector<glm::vec3> vertices;
    std::vector<glm::vec3> normals;
    std::vector<glm::vec3> tangents;
    std::vector<glm::vec2> texcoords;

    for (auto x = 0; x < _maze->x(); ++x)
    {
        for (auto y = 0; y < _maze->y(); ++y)
        {
            makeRectangle(vertices, normals, texcoords, tangents,
                          glm::vec3(x + 1, y, 1), glm::vec3(x, y, 1),
                          glm::vec3(x, y + 1, 1), glm::vec3(x + 1, y + 1, 1), glm::vec3(0, 0, -1), glm::vec3(1, 0, 0));
        }
    }

    DataBufferPtr buf0 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    buf0->setData(vertices.size() * sizeof(float) * 3, vertices.data());

    DataBufferPtr buf1 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    buf1->setData(normals.size() * sizeof(float) * 3, normals.data());

    DataBufferPtr buf2 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    buf2->setData(texcoords.size() * sizeof(float) * 2, texcoords.data());

    DataBufferPtr buf3 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    buf3->setData(tangents.size() * sizeof(float) * 3, tangents.data());

    _ceilMesh = std::make_shared<Mesh>();
    _ceilMesh->setAttribute(0, 3, GL_FLOAT, GL_FALSE, 0, 0, buf0);
    _ceilMesh->setAttribute(1, 3, GL_FLOAT, GL_FALSE, 0, 0, buf1);
    _ceilMesh->setAttribute(2, 2, GL_FLOAT, GL_FALSE, 0, 0, buf2);
    _ceilMesh->setAttribute(3, 3, GL_FLOAT, GL_FALSE, 0, 0, buf3);
    _ceilMesh->setPrimitiveType(GL_TRIANGLES);
    _ceilMesh->setVertexCount(vertices.size());

    _ceilMesh->setModelMatrix(glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, 0.0f, 0.0f)));
}

void MazeApplication::createWalls()
{
    std::vector<std::vector<int> > walls = _maze->getWalls();
    auto floor = _maze->getFloor();

    int _x = _maze->x();
    int _y = _maze->y();

    for (auto x = 0; x < _x; ++x)
    {
        for (auto y = 0; y < _y; ++y)
        {
            if (x == 0 && walls[x][y])
            {
                auto curMesh = makeMeshRectangle(glm::vec3(x, y + 1, 0), glm::vec3(x, y + 1, 1),
                                                 glm::vec3(x, y, 1), glm::vec3(x, y, 0), glm::vec3(1, 0, 0),  glm::vec3(0, 1, 0));
                curMesh->setModelMatrix(glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, 0.0f, 0.0f)));
                _wallMeshes.emplace_back(curMesh, walls[x][y]);
            }

            if (x == _x - 1 && walls[x][y])
            {
                auto curMesh = makeMeshRectangle(
                        glm::vec3(x + 1, y, 0), glm::vec3(x + 1, y, 1),
                        glm::vec3(x + 1, y + 1, 1), glm::vec3(x + 1, y + 1, 0), glm::vec3(-1, 0, 0),  glm::vec3(0, 1, 0));
                curMesh->setModelMatrix(glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, 0.0f, 0.0f)));
                _wallMeshes.emplace_back(curMesh, walls[x][y]);
            }

            if (y == 0 && walls[x][y])
            {
                auto curMesh = makeMeshRectangle(
                        glm::vec3(x, y, 0), glm::vec3(x, y, 1),
                        glm::vec3(x + 1, y, 1), glm::vec3(x + 1, y, 0), glm::vec3(0, -1, 0),  glm::vec3(1, 0, 0));
                curMesh->setModelMatrix(glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, 0.0f, 0.0f)));
                _wallMeshes.emplace_back(curMesh, walls[x][y]);
            }

            if (y == _y - 1 && walls[x][y])
            {
                auto curMesh = makeMeshRectangle(
                        glm::vec3(x + 1, y + 1, 0), glm::vec3(x + 1, y + 1, 1),
                        glm::vec3(x, y + 1, 1), glm::vec3(x, y + 1, 0), glm::vec3(0, 1, 0),  glm::vec3(1, 0, 0));
                curMesh->setModelMatrix(glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, 0.0f, 0.0f)));
                _wallMeshes.emplace_back(curMesh, walls[x][y]);
            }

            if (floor[x][y])
            {
                if (y > 0)
                    if (walls[x][y - 1]) {
                        auto curMesh = makeMeshRectangle(
                                glm::vec3(x, y, 0), glm::vec3(x, y, 1),
                                glm::vec3(x + 1, y, 1), glm::vec3(x + 1, y, 0), glm::vec3(0, 1, 0),  glm::vec3(1, 0, 0));
                        curMesh->setModelMatrix(glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, 0.0f, 0.0f)));
                        _wallMeshes.emplace_back(curMesh, walls[x][y - 1]);
                    }
                if (y < _y - 1)
                    if (walls[x][y + 1])
                    {
                        auto curMesh =  makeMeshRectangle(
                                glm::vec3(x + 1, y + 1, 0), glm::vec3(x + 1, y + 1, 1),
                                glm::vec3(x, y + 1, 1), glm::vec3(x, y + 1, 0), glm::vec3(0, -1, 0),  glm::vec3(1, 0, 0));
                        curMesh->setModelMatrix(glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, 0.0f, 0.0f)));
                        _wallMeshes.emplace_back(curMesh, walls[x][y + 1]);
                    }
                if (x > 0)
                    if (walls[x - 1][y])
                    {
                        auto curMesh = makeMeshRectangle(
                                glm::vec3(x, y + 1, 0), glm::vec3(x, y + 1, 1),
                                glm::vec3(x, y, 1), glm::vec3(x, y, 0), glm::vec3(1, 0, 0), glm::vec3(0, 1, 0));
                        curMesh->setModelMatrix(glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, 0.0f, 0.0f)));
                        _wallMeshes.emplace_back(curMesh, walls[x - 1][y]);
                    }
                if (x < _x - 1)
                    if (walls[x + 1][y])
                    {
                        auto curMesh = makeMeshRectangle(
                                glm::vec3(x + 1, y, 0), glm::vec3(x + 1, y, 1),
                                glm::vec3(x + 1, y + 1, 1), glm::vec3(x + 1, y + 1, 0), glm::vec3(-1, 0, 0), glm::vec3(0, 1, 0));
                        curMesh->setModelMatrix(glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, 0.0f, 0.0f)));
                        _wallMeshes.emplace_back(curMesh, walls[x + 1][y]);
                    }
            }
        }
    }

}



void MazeApplication::makeRectangle(std::vector<glm::vec3>& vertices, std::vector<glm::vec3>& normals,
                                    std::vector<glm::vec2>& texcoords, std::vector<glm::vec3>& tangents,
                                    glm::vec3 p1, glm::vec3 p2, glm::vec3 p3, glm::vec3 p4, glm::vec3 n, glm::vec3 t)
{
    vertices.push_back(p1);
    vertices.push_back(p2);
    vertices.push_back(p3);

    vertices.push_back(p1);
    vertices.push_back(p3);
    vertices.push_back(p4);

    for(auto i = 0; i < 6; ++i)
        normals.push_back(n);

    for(auto i = 0; i < 6; ++i)
        tangents.push_back(t);

    texcoords.push_back(glm::vec2(0.0, 0.0));
    texcoords.push_back(glm::vec2(0.0, 1.0));
    texcoords.push_back(glm::vec2(1.0, 1.0));

    texcoords.push_back(glm::vec2(0.0, 0.0));
    texcoords.push_back(glm::vec2(1.0, 1.0));
    texcoords.push_back(glm::vec2(1.0, 0.0));
}

MeshPtr MazeApplication::makeMeshRectangle(glm::vec3 p1, glm::vec3 p2, glm::vec3 p3, glm::vec3 p4, glm::vec3 n, glm::vec3 t)
{
    std::vector<glm::vec3> vertices;
    std::vector<glm::vec3> normals;
    std::vector<glm::vec2> texcoords;
    std::vector<glm::vec3> tangents;

    makeRectangle(vertices, normals, texcoords, tangents, p1, p2, p3, p4, n, t);

    DataBufferPtr buf0 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    buf0->setData(vertices.size() * sizeof(float) * 3, vertices.data());

    DataBufferPtr buf1 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    buf1->setData(normals.size() * sizeof(float) * 3, normals.data());

    DataBufferPtr buf2 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    buf2->setData(texcoords.size() * sizeof(float) * 2, texcoords.data());

    DataBufferPtr buf3 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    buf3->setData(tangents.size() * sizeof(float) * 3, tangents.data());

    MeshPtr mesh = std::make_shared<Mesh>();
    mesh->setAttribute(0, 3, GL_FLOAT, GL_FALSE, 0, 0, buf0);
    mesh->setAttribute(1, 3, GL_FLOAT, GL_FALSE, 0, 0, buf1);
    mesh->setAttribute(2, 2, GL_FLOAT, GL_FALSE, 0, 0, buf2);
    mesh->setAttribute(3, 3, GL_FLOAT, GL_FALSE, 0, 0, buf3);
    mesh->setPrimitiveType(GL_TRIANGLES);
    mesh->setVertexCount(vertices.size());

    return mesh;
}